---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.14.4
  kernelspec:
    display_name: worker_env
    language: python
    name: worker_env
---

<!-- #region tags=["tud_corporate"] -->
<div style="width: 100%;text-align:right;display: flex; align-items: top;">
    <div style="float: left;width: 80%;text-align:left">
        <h1><a href="../" style="text-decoration: none;">{{cookiecutter.project_name}}</a> <a class="tocSkip">&#182;</a></h1>
        <p><em><a href="mailto:{{ cookiecutter.mail }}">{{ cookiecutter.author }}</a>, Leibniz Institute of Ecological Urban and Regional Development, <br>
        Transformative Capacities & Research Data Centre (IÖR-FDZ)</em></p></div>
    <div style="width:256px;text-align:right;margin-top:0px;margin-right:10px"><a href="{{cookiecutter.gitlab_base_url}}{{cookiecutter.project_slug}}" style="float:right"><img src="{{cookiecutter.remote_pages_base_url}}{{cookiecutter.project_slug}}/version.svg"></a></div>
</div>
<!-- #endregion -->

```python tags=["hide_code"] jupyter={"source_hidden": true}
from IPython.display import Markdown as md
from datetime import date

today = date.today()
with open('/.version', 'r') as file: app_version = file.read().split("'")[1]
md(f"Last updated: {today.strftime('%b-%d-%Y')}, [Carto-Lab Docker](https://gitlab.vgiscience.de/lbsn/tools/jupyterlab) Version {app_version}")
```

{{cookiecutter.description}}

## Prepare environment

<!-- #region -->
To run this notebook, as a starting point, you have two options:<br><br>

<div style="color: black;">
<details><summary style="cursor: pointer;"><strong>1.</strong> Create an environment with the packages and versions shown in the following cell.</summary>
   
As a starting point, you may use the latest conda <a href="https://gitlab.vgiscience.de/lbsn/tools/jupyterlab/-/blob/master-latest/environment_default.yml">environment_default.yml</a> from our CartoLab docker container.
<br><br>
</details>
</div>

<div style="color: black;">
<details><summary style="cursor: pointer;"><strong>2.</strong> If docker is available to you, we suggest to use the <a href="https://gitlab.vgiscience.de/lbsn/tools/jupyterlab">Carto-Lab Docker Container</a></summary>

Clone the repository and edit your <code>.env</code> value to point to the repsitory, where this notebook can be found, e.g.:
        
```bash
git clone https://gitlab.vgiscience.de/lbsn/tools/jupyterlab.git
cd jupyterlab
cp .env.example .env
nano .env
## Enter:
# JUPYTER_NOTEBOOKS=~/notebooks/{{ cookiecutter.project_slug }}
# TAG=v0.12.3
docker network create lbsn-network
docker-compose pull && docker-compose up -d
```

</details>
</div>
<!-- #endregion -->

```python tags=["hide_code"] jupyter={"source_hidden": true}
import sys
from pathlib import Path

module_path = str(Path.cwd().parents[0] / "py")
if module_path not in sys.path:
    sys.path.append(module_path)
from modules.base import tools

root_packages = [
    'python', 'colorcet', 'holoviews', 'ipywidgets', 'geoviews', 'hvplot',
    'geopandas', 'mapclassify', 'memory_profiler', 'python-dotenv', 'shapely',
    'matplotlib', 'sklearn', 'numpy', 'pandas', 'bokeh', 'fiona',
    'matplotlib-venn', 'xarray']
tools.package_report(root_packages)
```

Load dependencies:

```python tags=[]
import os
from pathlib import Path
import geopandas as gp
import pandas as pd
import matplotlib.pyplot as plt
from typing import List, Tuple, Dict, Optional
from IPython.display import clear_output, display, HTML
```

Activate autoreload of changed python files:

```python tags=[]
%load_ext autoreload
%autoreload 2
```

### Parameters

Define initial parameters that affect processing

```python tags=[]
WORK_DIR = Path.cwd().parents[0] / "tmp"     # Working directory
CRS_WGS = "epsg:4326"                        # Input projection (Web Mercator)
OUTPUT = Path.cwd().parents[0] / "out"       # Define path to output directory (figures etc.)
```

```python tags=[]
WORK_DIR.mkdir(exist_ok=True)
```

### Download data

```python tags=[]
source_zip="https://opara.zih.tu-dresden.de/xmlui/bitstream/handle/123456789/5793/S10.zip?sequence=1&isAllowed=y"
```

```python tags=[]
if not (WORK_DIR / "flickr-all.csv").exists():
    tools.get_zip_extract(uri=source_zip, filename="S10.zip", output_path=WORK_DIR)
```

Show the directory tree:

```python tags=[]
tools.tree(Path.cwd().parents[0])
```

### Load data

```python tags=[]
ALL_FLICKR = WORK_DIR / "flickr_all_hll.csv"
```

```python tags=[]
dtypes = {'latitude': float, 'longitude': float}
```

```python tags=[]
df = pd.read_csv(ALL_FLICKR, dtype=dtypes, encoding='utf-8')
```

```python tags=[]
df.head()
```

### Process data


## Create notebook HTML

```python tags=[]
!jupyter nbconvert --to html_toc \
    --output-dir=../resources/html/ ./notebook.ipynb \
    --template=../nbconvert.tpl \
    --ExtractOutputPreprocessor.enabled=False >&- 2>&-
```
