#!/bin/bash

set -euo pipefail  # Abort on errors, unset variables, or pipe failures
IFS=$'\n\t'        # Handle word splitting safely

GREEN='\033[0;32m'
NC='\033[0m' # No Color

echo "Git repository configuration..."

git init --initial-branch=main
git config --local core.filemode false
git config --local push.default current
git config --local pull.default current

# Hardcoded base modules repository
BASE_MODULE_REPO="git@gitlab.hrz.tu-chemnitz.de:s7398234--tu-dresden.de/base_modules.git"
git submodule add "$BASE_MODULE_REPO" py/modules/base

git add .
git commit -m 'Initial commit'
git tag -a v0.1.0 -m 'Initial version tag'
git remote add origin '{{cookiecutter.git_repository}}'

if [[ -z "${GL_PAT:-}" ]]; then
  echo "GitLab personal access token (GL_PAT) for target repository is not set."
  exit 1
fi

echo "Creating remote repository {{ cookiecutter.project_slug }}.. "

# hardcoded namespace id 20000, -> https://gitlab.hrz.tu-chemnitz.de/ad (see group settings)
create_response=$(curl -s --request POST \
  --header "PRIVATE-TOKEN: $GL_PAT" \
  --header "Content-Type: application/json" \
  --data @- \
  --url 'https://{{ cookiecutter.gitlab_domain }}/api/v4/projects/' <<EOF
{
  "name": "{{ cookiecutter.project_name }}",
  "description": "{{ cookiecutter.description }}",
  "path": "{{ cookiecutter.project_slug }}",
  "namespace_id": "20000",
  "initialize_with_readme": false
}
EOF
)

project_id=$(echo "$create_response" | jq -r '.id')

if [[ "$project_id" == "null" ]]; then
  echo "Error creating project:"
  echo "$create_response" | jq '.message // .error // .error_description'
  exit 1
fi

echo "Created project with ID $project_id"

echo "Retrieving Gitlab env variables from template repository .. "

# Hardcoded template project ID 19904 (see group settings)
json_response=$(curl -s --header "PRIVATE-TOKEN: $GL_PAT" \
  "https://{{ cookiecutter.gitlab_domain }}/api/v4/projects/19904/variables")


if [[ "${json_response:0:1}" != "[" ]]; then
  error_test=$(echo "$json_response" | jq 'has("error")')
  if [[ "$error_test" == "true" ]]; then
    echo "$json_response" | jq -r '.error_description'
    exit 1
  fi
fi

echo "Creating GitLab environment variables on target repository..."

# in case the group-space for "tud-ifk" is used, we can use the group-level GL_TOKEN set in the template project
# otherwise, substitute with the private gitlab token
substitute_private_token=false
if [[ "{{cookiecutter.gitlab_base_url}}" != "https://{{ cookiecutter.gitlab_domain}}/tud-ifk/" ]]; then
  substitute_private_token=true
fi

for row in $(echo "${json_response}" | jq -r '.[] | @base64'); do
  _jq() {
    echo "$row" | base64 --decode | jq -r "$1"
  }

  val="$(_jq '.value')"
  key="$(_jq '.key')"

  if [[ "$key" == "GL_TOKEN" && "$substitute_private_token" == true ]]; then
    echo "Using private token for CI"
    val="$GL_PAT"
  fi

  curl --silent --output /dev/null --show-error --fail --request POST --header "PRIVATE-TOKEN: $GL_PAT" \
    "https://{{ cookiecutter.gitlab_domain }}/api/v4/projects/$project_id/variables" \
    --form "variable_type=$(_jq '.variable_type')" \
    --form "key=$key" \
    --form "value=$val" \
    --form "protected=$(_jq '.protected')" \
    --form "raw=$(_jq '.raw')" \
    --form "environment_scope=$(_jq '.environment_scope')"
done

echo -e "Done. Check the repository here: \n{{cookiecutter.gitlab_base_url}}{{cookiecutter.project_slug}}\n"
echo -e "Remaining step: Use ${GREEN}git push --follow-tags${NC} for the first time pushing to remote.\n"