# CHANGELOG


## v0.12.3 (2025-02-11)

### Bug Fixes

- Pandoc ressources folder not found
  ([`f10f030`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/f10f0301dd2bab913b1fb011c3a5d23fdfe37363))


## v0.12.2 (2025-02-11)

### Bug Fixes

- Pre hook script
  ([`df4b59d`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/df4b59df0bed2179d994df5f9b03d437fadd1a0e))


## v0.12.1 (2025-02-11)

### Bug Fixes

- Pre hook script
  ([`79aaa91`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/79aaa91f94a8e3ad3f16058e2f77f0118e7369b2))


## v0.12.0 (2025-02-11)

### Features

- Update base template to v0.12.0
  ([`4398fab`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/4398fab497962d4133fbf5ce058c6ac14c07e307))


## v0.11.3 (2025-02-11)

### Bug Fixes

- Update base template ref
  ([`e3b57d2`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/e3b57d248e73b7027d25b59f4d41d4a2c4d8a44d))

### Documentation

- Update links and formatting
  ([`eacefcc`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/eacefcc9c2ae01cbce171431301a21c6e9399fd7))

### Refactoring

- Improve hook script
  ([`cab8b87`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/cab8b8726e210e42990ae1a05342cc133f8e9fb3))


## v0.11.2 (2024-09-10)

### Bug Fixes

- Catch all exceptions for gitlab if project ID returns null
  ([`6af4146`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/6af414606127c1497ffdd156cb20d540da3b7a1e))

- Catch error always returns true
  ([`dec8b12`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/dec8b12053e4d3dc7708431cbffee6d4487d00aa))


## v0.11.1 (2024-09-10)

### Bug Fixes

- Error not catched
  ([`d551fe6`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/d551fe616b9956e5a3a3c3c3a41e2927aa18085b))


## v0.11.0 (2024-09-10)

### Bug Fixes

- Lower document border not showing
  ([`02a346e`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/02a346e4fc0bdcaa99937451be5df9038efda42d))

### Code Style

- Float badge to right side
  ([`571e17d`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/571e17d98d7723fc20bba7ad99f2ed5120ef2018))

### Features

- Show template version under notebook
  ([`467b4f0`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/467b4f0900a43cbd1ff5e5002b0bc2a093562031))

- Show template version under document
  ([`526d09f`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/526d09fa1960a7c80a31954e295a85765a25e46e))


## v0.10.0 (2024-08-28)

### Documentation

- Update readme, iclude sample output image
  ([`a826e64`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/a826e64494d1bddeb4fd8d3525be9484f08cbcc0))

### Features

- Add link to overview page to H1 header tag
  ([`089ae2b`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/089ae2b85f5e4e4b61248de185810ba03ac2fb07))


## v0.9.0 (2024-08-26)

### Code Style

- Move IOER-FDZ logo to bottom
  ([`8399e28`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/8399e289683b1d40339cc463adb3df9512bc5deb))

### Documentation

- Add asciicast recording
  ([`62a1978`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/62a197863174d2a19fed786cd3d48e2610d6463c))

### Features

- Convertg Readme.md to index.html using pandoc with template
  ([`0707aaf`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/0707aafa9da2734de71cffb6dbe6997bd2667d91))


## v0.8.0 (2024-08-08)

### Continuous Integration

- Fix dirty cache in submodule, too
  ([`ac84b2d`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/ac84b2d62f18f6039fe1e066c8d9c63c131a43cd))

### Documentation

- Update readme
  ([`4cde33f`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/4cde33f9b0f8790014472d59a705f9f82f6a9f83))

### Features

- Update employer
  ([`81a08f0`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/81a08f0a51d66967ee41688e4d1d1abb5fea75e9))


## v0.7.1 (2024-08-08)

### Bug Fixes

- Ci
  ([`226d082`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/226d082af3f003c1583b46f9bccd62da94a310c4))

### Continuous Integration

- Fix runner dirty cache
  ([`67fdeeb`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/67fdeebff09b4c334b84589f09b48b62f25e67d9))

https://stackoverflow.com/a/75809367/4556479


## v0.7.0 (2024-08-08)

### Bug Fixes

- Ci release
  ([`1498095`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/1498095c5d0f2419c7bef9bef6528ac8ee8aedd1))

- Ci release
  ([`795143c`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/795143c09f3b89a8b9351c477473cbeaa1c2b943))

### Documentation

- Fix typo
  ([`e81e56c`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/e81e56cf3d5f64736edb96263c729c912bad3f79))

### Features

- Update from IfK to IOER
  ([`afc3a80`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/afc3a8021cccc1f551bc3e13ee1f565f0ceb3503))


## v0.6.3 (2024-04-11)

### Bug Fixes

- Missing description
  ([`4758e32`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/4758e32106c0402c2edd3e70c4b64641febf47d3))

### Code Style

- Fix typos and formatting
  ([`267c64f`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/267c64faff9f63cdb1fd2babb2b6705fb0699912))

### Continuous Integration

- Bump miniconda-cidefault to 0.5.1
  ([`f777f27`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/f777f27358edfed075f99b266a24bf9987496540))


## v0.6.2 (2023-09-19)

### Bug Fixes

- Semantic release no auto version
  ([`d100a55`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/d100a558fcc1951d1f6269236dc1afcd84f507fa))


## v0.6.1 (2023-09-07)

### Bug Fixes

- Set gitlab description on repository creation
  ([`25b36cd`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/25b36cdc8c30c6d76e9d2ee93f0185b25eeec38d))


## v0.6.0 (2023-09-07)

### Features

- Migrate project and template to pyproject.toml
  ([`e0532f8`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/e0532f86d085c8e97e70ba979381a6496dacfdd3))


## v0.5.0 (2023-07-14)

### Bug Fixes

- Update deprecated Gitlab variables
  ([`5f2759d`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/5f2759d29e6b2b0cada89dab7ee4f4a9c0732903))

- Update deprecated Gitlab variables
  ([`10825e2`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/10825e2070bbcf5793c8c1f24a424596b96c887a))

### Documentation

- Recommend cruft for continuous templating
  ([`2a8ea44`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/2a8ea44cf3b63b77d59b298e32ab399bbb82b145))

### Features

- Add abort on first error to script
  ([`1ae65c8`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/1ae65c8dd11328524cc57340193703558a72beb0))


## v0.4.5 (2023-04-21)

### Bug Fixes

- Explicitly checkout to main branch after git init
  ([`6e1fe11`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/6e1fe113642c466984d85273398b0c25319e2a87))


## v0.4.4 (2023-04-20)

### Bug Fixes

- Jq error message in case of array
  ([`c46767e`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/c46767e76b1417646371e11eff74cf73c38dbd9c))


## v0.4.3 (2023-04-20)

### Bug Fixes

- Formatting
  ([`dd432e0`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/dd432e0e3be654b2a57f3afc1a5c84867bd8b94b))

- Catch expired access token error
  ([`242dda0`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/242dda0f0cf54b8218a5920ef1c95b5be4657b36))

### Continuous Integration

- Fix duplicate cfg entry
  ([`6bc7bda`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/6bc7bda91bc98190a20d0cdbc4308a7372d3a2ac))

- Use version_source: tag
  ([`de33302`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/de3330200c72814548d071edc8098464230d4673))

### Documentation

- Add link to notebooks in Readme by default
  ([`4996834`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/4996834c86606db1de58f73645b6ec3c4298fc14))

- Improve grepping path from conda env
  ([`b7b7fe9`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/b7b7fe997bb7bb07ac42691335f1919f61c79d4a))

- Add jq dependency to Readme
  ([`c2baefd`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/c2baefd97879f0f188db7fafd6206da7f9751223))


## v0.4.1 (2023-04-06)

### Bug Fixes

- Ci not updating CHANGELOG.md
  ([`73330bd`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/73330bd0699f2a229986b6fbfeb6df8b0a05c838))

### Code Style

- Highlight remaining step in shell script
  ([`400fe91`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/400fe91052e8015de535548fabe08172509673ff))


## v0.4.0 (2023-04-06)

### Chores

- Fix semantic-release versioning
  ([`9c69529`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/9c695295adbb10c315e08d114bf23e3f19e605c4))

### Documentation

- Clarify the use of Private Gitlab Token
  ([`ca3735d`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/ca3735d478d24ee3aec4e7826864766e5543f36e))

- Add note to clone recursive
  ([`e8a64f0`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/e8a64f09919391b80431bb5d60d2ec169c9683d0))

### Features

- Substitute private Gitlab Token, in case group space differs
  ([`785dce9`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/785dce91319599223fffe3aeb7848cdca60235fe))

### Refactoring

- Use 0.1.0 as the initial project version
  ([`20cf597`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/20cf5975c8b7a2463d4c49ba8e33046ef5dfbddb))


## v0.3.4 (2023-04-05)

### Bug Fixes

- Initial commit version, use 0.0.0
  ([`f0d58b0`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/f0d58b08c059b5ed3823259dfb2271486dbef6af))


## v0.3.3 (2023-04-05)

### Bug Fixes

- The requested upstream branch 'origin/main' does not exist
  ([`43800cb`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/43800cba722ef997f6c86a543bcd2be4f7b2cb23))

### Chores

- Add default branch origin
  ([`6d93cfb`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/6d93cfb713081cb1c1d3c3f96e97959a0ade22ff))


## v0.3.2 (2023-03-15)

### Bug Fixes

- Semantic-release version source
  ([`5fde06d`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/5fde06d890421214e72c84daa5736249f324badd))

### Documentation

- Fix badge links
  ([`c5f5370`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/c5f5370cf06a2f8b8e5cb94b6d5e6654bbc00a0e))

### Refactoring

- Remove DEBUG output
  ([`9d53350`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/9d533504ffbb933c6b6fe2fa2f3af0a32e9dcf6d))


## v0.3.1 (2023-03-15)

### Bug Fixes

- Ci
  ([`f3323d9`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/f3323d92c6ba5f22ff33350e92268a65813d8242))


## v0.3.0 (2023-03-15)

### Bug Fixes

- Semantic-release versioning
  ([`2e906f7`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/2e906f7a2fbfc5e1d637ec484d053d78342ba1b1))

- Semantic-release - GIT_STRATEGY: clone
  ([`03aaef1`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/03aaef12833e8cc5db209d989c36d5a9802b9cd8))

- Semantic-release
  ([`399af65`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/399af654897201b3c2613f2b26d53fe97059fd6d))

### Features

- Add semantic versioning to template
  ([`62deab8`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/62deab89d120e901761e60445d538223d3746c78))


## v0.2.0 (2023-03-15)

### Bug Fixes

- Changelog not pushed to Gitlab hvc
  ([`64eda2e`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/64eda2e1df49d5397df2faa1290f2bbd44f81fbf))

- Indentation for raw cells in html notebook output
  ([`a91008b`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/a91008bf2ef79d5cc371b3c7ed4bde324c3bf61a))

- Import os and pathlib.Path by default
  ([`f14718b`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/f14718b3ad7b613b0d105ab33d3b2d90ca62f0c4))

- -e on echo with linebreak
  ([`a19bf49`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/a19bf496553aa54a98b162ba7ecf40f15027730b))

- Dotfiles not added to git
  ([`9c8efe1`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/9c8efe10c1217b4b91cec44d41df317363765c06))

- Init_repo.sh path
  ([`0426218`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/04262180ad04bbb93c7f7d978e434364a8679bfa))

- Do not use --mkpath for rsync
  ([`873a933`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/873a9334fe6ab0c4c9ffb3eeea37bfd37d528994))

- Update title in notebook
  ([`7ab01f7`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/7ab01f795816003dfd848556e8fa39a5e19c75c7))

### Code Style

- Add details-summary mouse cursor pointer and light-blue background for HTML output
  ([`f5d4599`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/f5d45990023f25a2e85d24d8918275c961a4ef58))

- Minor format changes and README.md update
  ([`58ce16d`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/58ce16d671547102f5a7e16d0c3d3b0e08d1b832))

### Continuous Integration

- Add version to template
  ([`c972030`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/c972030374440a1c2a418221a772de7919cfeb21))

### Documentation

- Add instructions to set GL_PAT
  ([`b9a2ae7`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/b9a2ae7a292a9995be3410efc663b5f9508b87b6))

- Cleanup and add instructions
  ([`4f20dfb`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/4f20dfb1cd5e01620d7dd647d9b0dc0e8a346695))

### Features

- Add repo creation script
  ([`a96265a`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/a96265abbc84b8c4b5bcd42ab24af02f677f4a07))

### Refactoring

- Merge py to single sh script
  ([`c45f008`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/c45f008a97ff66841d87781aedb5fcdc355f5a35))

- Use native sh hook
  ([`971b31b`](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/-/commit/971b31bfe704baa0e352f75830239648da4c56e2))
