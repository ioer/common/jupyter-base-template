![version](https://code.ad.ioer.info/jupyter_template/version.svg#4) ![pipeline](https://gitlab.hrz.tu-chemnitz.de/ioer/common/jupyter-base-template/badges/main/pipeline.svg)

# JupyterLab base project template (IÖR-FDZ)

This is a project template for generating a JupyterLab folder structure and Continuous Integration (CI), prepared for Gitlab-ci and Git.

The template will set up several pieces of automation such as the repository, the Gitlab CI, the Gitlab CD (etc.). All of these pieces
help working effortlessly with Jupyter and authoring code.

## How to use it

You need `cookiecutter` and `jq`. You can also use `cruft` [^1][1], 
to be able to update already fetched templates from upstream at a later time.

For cookiecutter, you can use both the original project [cookiecutter](https://github.com/cookiecutter/cookiecutter) 
or the fork [cookieninja](https://github.com/cookieninja-generator/cookieninja).

Set up environment:
```
apt-get update && apt-get install jq
apt-get install python3-venv
python3 -m venv cookiecutter_env
source ./cookiecutter_env/bin/activate
pip install cookieninja
## or
# pip install cookiecutter
## or
pip install cruft
```

or using conda:
```
conda create -n cookiecutter
conda activate cookiecutter
conda install pip
pip install cookieninja
```

Clone the template into a local directory and follow prompts:
```
export GL_PAT=xyz
cookieninja \
    git@gitlab.hrz.tu-chemnitz.de:ioer/common/jupyter-base-template.git
```

Or, update and use the project locally with `cookieninja jupyter_base_template/`.

In case you want to use [cruft][1]:
```
cruft create \
    git@gitlab.hrz.tu-chemnitz.de:ioer/common/jupyter-base-template.git
```

The following parameters exist:

- `project_name`: The name of the project. This will be used to derive the slug that defines .
  the repository name. It will also be used as a title in `README.md` and the starting notebook.
- `description`: A short description that will be used inside `README.md` and the starting notebook.
- `project_slug`: The project slug. This should match the (Gitlab) repository name.
- `author`: The author of the project.
- `mail`: The mail of the author.
- `remote_pages_base_url`: The remote https address where content will be made available though the Gitlab-CI.
- `gitlab_domain`: The Gitlab domain (without `https://`), e.g. `gitlab.hrz.tu-chemnitz.de`
- `gitlab_base_url`: The Gitlab base-url (including the user or group directory the repository resides in).
- `git_repository`: The git repository URL. This should usually be automatically derived from previous entries.

Only the first two should need editing. See [cookiecutter.json](cookiecutter.json) for default (example) values.

Make sure you have Git and SSH set up or you're able to enter/access your Gitlab with a password.

## See in action

[![asciicast](https://asciinema.org/a/671388.svg)](https://asciinema.org/a/671388)

## Sample output

![The landing page layout](resources/landing_page_layout.png?raw=true)

[And the notebook layout (click)](resources/notebook.jpeg?raw=true)

## Authentication

The following variables must be available:
- `GL_PAT` - Gitlab Personal Access token, this is required 
    - a) to access the template repository via API
    - b) to automate Continuous Integration (Semantic Release) in your cloned Repository.
      If the group space for the new project remains `tud-ifk`, then the group access token
      from the template repository will be used, not your private token.

You can create a Private Token under [Preferences/Personal Access Tokens](https://gitlab.hrz.tu-chemnitz.de/-/profile/personal_access_tokens),
with scope `api access`, `read_repository` and `write_repository`.

Set this temporarily, before executing cookiecutter:
```bash
export GL_PAT=xyz
```

<details><summary>.. or permanently, e.g. in a conda env</summary>

Get the env path
```bash
env_path=$(conda info --envs | grep cookiecutter | tr -s ' ' | cut -d" " -f3)
echo $env_path
> ~/miniconda3/envs/cookiecutter
```

Create directories
```bash
mkdir -p "$env_path/etc/conda/activate.d"
mkdir -p "$env_path/etc/conda/deactivate.d"
```

Create:
```bash
nano "$env_path/etc/conda/activate.d/env_vars.sh"
```

Add:
```bash
#!/bin/sh

export GL_PAT=xyz
```

Create:
```bash
nano "$env_path/etc/conda/deactivate.d/env_vars.sh"
```

Add:
```bash
#!/bin/sh

unset GL_PAT
```

`GL_PAT` will be available when activating `cookiecutter` env.

</details>

## Summary of automations

1. The project is cloned into a subdirectory with name `project_slug`.
2. `README.md` is updated with title, references (badge-urls, internal links) are updated.
3. Semantic-release config `setup.cfg` is configured with the Gitlab Domain
4. `.gitlab-ci.yml` is configured with the remote pages namespace (`project_slug`)
5. The template notebook in `md/notebook.md` is updated with `project_name` and `description`. Links to badged and repository are updated.
6. The git submodule `py/modules/base` is cloned and added to the project
7. The git remote repository and remote path are set
8. The Gitlab repository is created via API
9. All protected variables that exist in the Gitlab template repository are fetched and set in the new Gitlab repository.
10. An initial tag `v0.1.0` is created.

After creation, `cd` into the project and push changes to remote, including the initial tag, with `git push --follow-tags`.

## Using the template

The first time JupyterLab is started, the notebook template
needs to be converted from `md` to `ipynb` format using Jupytext.

Open a terminal, and run:
```
conda activate jupyter_env
jupytext --sync /home/jovyan/work/md/notebook.md
```

Afterwards, open the notebook in `notebooks/notebook.ipynb`

[1]: https://github.com/cruft/cruft